
(in-package :cl-x86-asm)

;; ----------- vector / section handing ------------------------------------------------
(defgeneric current-location-of (segment))

(defmethod current-location-of ((seg data-segment))
  "Return the offest at which the current instruction/data gets assembled"
  (length (contents-of seg)))

(defmethod current-location-of ((seg bss-segment))
  "Return the offset at which more space will be allocated"
  (bss-size-of seg))
  

;; our target - a collection of vectors for code and data, 
;; plus data area and symbol table
(defclass target-segments
    ()
  ;; will contain vector of raw data, hashcode symbol table
  ;; methods are emit-opcode, intern-pc-symbol,
  ((segments
   :initform (make-hash-table :test 'equal)
   :accessor segments-of)
   (current-segment
   :initform nil :accessor current-segment-name-of)
   (symbol-table
    :initform (make-instance 'symbol-table)
    :accessor symbol-table-of))
  (:documentation "A collection of segments representing the eventual contents of an ELF relocatable object file"))


(defmethod current-segment-of ((target target-segments))
  (gethash (current-segment-name-of target) (segments-of target)))

(defmethod new-data-segment ((target target-segments) name &optional alignment)
  (setf (gethash name (segments-of target)) 
	(make-instance 'data-segment 
		       :alignment (or alignment 2))))

(defmethod new-bss-segment ((target target-segments) name &optional alignment)
  (setf (gethash name (segments-of target)) 
	(make-instance 'bss-segment 
		       :alignment (or alignment 2))))

(defmethod get-segment ((target target-segments) name)
  (gethash name (segments-of target)))

(defmethod code-vector-of ((target target-segments))
  (gethash "code" (segments-of target)))

(defmethod data-vector-of ((target target-segments))
  (gethash "data" (segments-of target)))

(defmethod bss-vector-of ((target target-segments))
  (gethash "bss" (segments-of target)))

(defmethod write-segments-to-elf (file)
  (let ((file-pathname
    (if (stringp file)
	(parse-namestring file)
	file)))
    (format t "Writing ~A" file-pathname)))

(defgeneric make-label-in-target (target label-name))

;; create a new label in the current segment and location of the 
;; target
(defmethod make-label-in-target ((target target-segments) label-name)
  (add-symbol-to-table 
   (symbol-table-of target) 
   label-name 
   (current-segment-name-of target)
   (current-location-of (current-segment-of target))))
 
(defgeneric get-label-in-target (target label-name))

;;
;; retrieve a symbol-table-entry from the targets symbol table
;;
(defmethod get-label-in-target ((target target-segments) label-name)
  (get-sybol-from-table 
   (symbol-table-of target)
   label-name))

;;
;; Assuming the targets current segment is a data or code segment, emit data
;;
(defgeneric emit-data (target data &key label-name))

;;
;; Similar to the above, but the values in the list go into the data vector
;;
(defmethod emit-data ((target target-segments) data-list &key label-name)
  ;; emit a label if required
  (assert (typep (current-segment-of target) 'data-segment))
  (when label-name
    (make-instance 
     '(symbol-table-entry 
       :symbol-real-name label-name 
       :in-segment (current-segment-name-of target)
       :mangled-name (mangle-symbol-name label-name)
       :symbol-offset-value (current-location-of (current-segment-of target)))))
  (mapcar #'(lambda (b) (vector-push-extend b (current-segment-of target))) data-list))

(defgeneric emit-bss (target data-size &key label-name))

;;
;; Similar to the above, but extends the uninitalised segment by the
;; given amount
;;
(defmethod emit-bss ((target target-segments) data-size &key label-name)
  (assert (typep (current-segment-of target) 'bss-segment))
  (when label-name
    (make-instance 
     '(symbol-table-entry 
       :symbol-real-name label-name 
       :in-segment (current-segment-name-of target)
       :mangled-name (mangle-symbol-name label-name)
       :symbol-offset-value (current-location-of (current-segment-of target)))))
  (incf (bss-size-of (current-segment-of target)) data-size))


;; -- our actual target that code gets assembled into

(defvar *current-target* '(make-instance 'target-segments))
