
(in-package :cl-x86-asm)

;; -- symbol table handling -----------------------------------------------------

;; symbol table maps to cl package for each segment. we maintain a list
;; of packages so that we can iterate over all symbols as needed

(defparameter *x86-symbol-packages* nil)
(defparameter *current-segment-name* nil)
(defparameter *current-segment-package* nil)

;; segments  -----------------------------------------------------


(defclass segment
  ()
  ((alignment 
    :initform 2 
    :initarg :alignment 
    :reader alignment-of)
   (symbol-table
    :initform nil
    :accessor symbols-of))
  (:documentation "Base class for file segments"))

(defclass data-segment
    (segment)
  ((contents 
    :initform (make-array '(4096) 
			  :element-type '(unsigned-byte 8) 
			  :fill-pointer 0) 
    :accessor contents-of))
  (:documentation "An ELF segment containig actual data"))

(defclass bss-segment
    (segment)
  ((size :initform 0 :accessor bss-size-of))
  (:documentation "An ELF segment intialised at run/load time"))


(defun x86-symbol-package-name (segment-name)
  (concatenate 'string segment-name "-x86-symbol-package"))

(defun set-current-segment (segment-name)
  (setf *current-segment-name* segment-name)
  (setf *current-segment-package* 
	(find-package (x86-symbol-package-name segment-name))))

(defun make-segment (segment-name &key set-to-current segment-type)
  (assert (member segment-type '(data-segment bss-segment)))
  (let ((segment-package 
	 (make-package (x86-symbol-package-name segment-name)))
	(segment-object (make-instance segment-type)))
    ;; add segment-package to master list
    (push segment-package *x86-symbol-packages*)
    ;; add the package to the object slot
    (setf (symbols-of segment-object) segment-package)
    ;; bind a symbol in the package to the segment object
    (setf (intern "segment-object" segment-package) segment-object)
    (when set-to-current
      (set-current-segment segment-name))))

(defun get-segment-object (segment-name)
  (getf (intern "segment-object" *current-segment-package*)))

;; emit data to our current segment
(defun emit-data-to-segment (data)
  (map nil 
  
;; symbols

(defun set-symbol-property (symbol-name symbol-property symbol-property-value)
  (let ((package-symbol 
	 (intern symbol-name *current-segment-package*)))
    (setf (get package-symbol symbol-property nil) symbol-property-value)))
	
(defun get-symbol-property (symbol-name symbol-property)
  (let ((package-symbol 
	 (intern symbol-name *current-segment-package*)))
    (get package-symbol symbol-property nil)))

;; to do -- possibly want to list symbols for mapcaring over