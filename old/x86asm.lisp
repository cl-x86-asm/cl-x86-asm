(in-package :cl-x86-asm)	    

;;; ------- code generation macros and helpers ------------------------------------

(defconstant +LOCK-PREFIX+ #XF0)
(defconstant +REPNE/REPNZ-PREFIX+ #XF2)
(defconstant +REP-PREFIX+ #XF3)
(defconstant +REPE/REPZ-PREFIX #XF3) 

(defconstant +CS-SEGMENT-OVERRIDE-PREFIX+ #X2E)
(defconstant +SS-SEGMENT-OVERRIDE-PREFIX  #X36)
(defconstant +DS-SEGMENT-OVERRIDE-PREFIX  #X3E)
(defconstant +ES-SEGMENT-OVERRIDE-PREFIX  #X26)
(defconstant +FS-SEGMENT-OVERRIDE-PREFIX  #X64)
(defconstant +GS-SEGMENT-OVERRIDE-PREFIX  #X65)

(defconstant +OPERAND-SIZE-OVERRIDE-PREFIX+ #X66)
(defconstant +ADDRESS-SIZE-OVERRIDE-PREFIX+ #X67)


(defparameter *registers-32* (list :eax :ecx :edx :ebx :esp :ebp :esi :edi))
(defparameter *registers-16* (list :ax  :cx :dx  :bx  :sp  :bi  :si  :di))
(defparameter *registers-8* (list :al :cl :dl :bl :ah :ch :dh :bh))
(defparameter *registers-mmx* (list :mm0 :mm1 :mm2 :mm3 :mm4 :mm5 :mm6 :mm7))
(defparameter *registers-segment* (list :es :cs :ss :ds :fs :gs))
(defparameter *registers-fp* (list :st0 :st1 :st2 :st3 :st4 :st5 :st6 :st7))
(defparameter *registers-control* (list :cr0 :invalid :cr2 :cr3 :cr4))
(defparameter *registers-debug* (list :dr0 :dr1 :dr2 :dr3 :invalid :invalid :dr6 :dr7))
(defparameter *registers-test* (list :invalid :invalid :invalid :tr3 :tr4 :tr5 :tr6 :tr7))

;; x86 can assemble in one of two modes BITS32 or BITS16
(defparameter *current-bits* 32)

;; switch between modes
(defun switch-bits ()
  (if (= *current-bits-size* 32)
      (setf *current-bits-size* 16)
      (setf *current-bits-size* 32)))

;;given a register symbol return intel's code for it
(defun register-code (sym)
  "Test to see if a symbol is a register, return its code if so"
  (if (symbolp  sym)
      (or (position sym *registers-32*)
	  (position sym *registers-16*)
	  (position sym *registers-8*))))

;; create a mod/rm byte from the given components - convienence 
;; function for others to use
(defun encode-mod/rm (&key mod r/m register opcode)
  "(encode-mod/rm :key k :r/m rm :register register &opcode opcode)"
  (logior 
   (ash mod 6)
   (ash r/m 3)
   (if opcode
       opcode
       register)))

(defun encode-address (effective-address reg/opcode)
  ;; simple case - flat address with two registers
  (if (and (symbolp effective-address) (symbolp reg/opcode))
      (encode-mod/rm :mod #B11 :r/m (register-code effective-address) 
		     :register (register-code reg/opcode))))



(defun directivep (s)
  (assert (or (symbolp s) (stringp s)))
  (let ((string 
	 (if (symbolp s)
	     (string-downcase (symbol-name s))
	     s)))
    (member string *assembler-directives* &test #'string=)))

(defun last-char (str)
  (if (not (zerop (length str)))
      (char str (1- (length str)))
      " "))

(defun trim-last-char (str)
  (if (not (zerop (length str)))
      (subseq str 0 (1- (length str)))
      ""))

(defun first-char (str)
  (if (not (zerop (length str)))
      (char str (1- (length str)))
      " "))

(defun trim-first-char (str)
  (if (not (zerop (length str)))
      (subseq str 1 (length str))
      ""))
      
(defun assembler-label-p (asm-sym)
  "(assembler-label-p sym-or-string)
Test to see if the symbol matches the requirements for a label. 
Returns the label if it does, nil if not."
    (let ((symbol-string 
	   (if (symbolp asm-sym)
	       (symbol-name asm-sym)
	       asm-sym)))
      (if (char= (last-char symbol-string) #\:)
	  (trim-last-char symbol-string))))

(defun assembler-directive-p (asm-sym)
  "(assembler-label-p sym-or-string)
Test to see if the symbol matches the requirements for a label. 
Returns the label if it does, nil if not."
    (let ((symbol-string 
	   (if (symbolp asm-sym)
	       (symbol-name asm-sym)
	       asm-sym)))
      (if (char= (first-char symbol-string) #\.)
	  (trim-first-char symbol-string))))





