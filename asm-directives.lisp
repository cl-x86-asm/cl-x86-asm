
(in-package :cl-x86-asm)

(defparameter *asm-directives*
  (list
   (cons :DB 
	 (list (function (lambda (argument) (byte-p argument)))))
   (cons :DW
	 (list (function (lambda (argument) (word-p argument)))))
   (cons :DD 
	 (list (function (lambda (argument) (dword-p argument)))))
   (cons :DQ
	 (list (function (lambda (argument) (qword-p argument)))))
   (cons :DS
	 (list (function (lambda (argument) (stringp argument))))


(defun directive-db (argument) )
(defun directive-dw (argument) )
(defun directive-dd (argument) )
(defun directive-dq (argument) )
