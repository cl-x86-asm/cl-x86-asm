	.file	"main.c"
	.section	.rodata
.LC0:
	.string	"The answer is %s "
	.text
.globl main
	.type	main, @function
main:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$24, %esp
	andl	$-16, %esp
	movl	$0, %eax
	addl	$15, %eax
	addl	$15, %eax
	shrl	$4, %eax
	sall	$4, %eax
	subl	%eax, %esp
	movl	$42, -4(%ebp)
	movl	-4(%ebp), %eax
	movl	%eax, 8(%esp)
	movl	$.LC0, 4(%esp)
	movl	stdout, %eax
	movl	%eax, (%esp)
	call	fprintf
	movl	$0, (%esp)
	call	exit
	.size	main, .-main
	.section	.note.GNU-stack,"",@progbits
	.ident	"GCC: (GNU) 3.4.6 (Gentoo 3.4.6-r1, HTB-3.4.4-1.00, ssp-3.4.5-1.0, pie-8.7.9)"
