
;; need to generate all possible jump instructions from
(defparameter *conditional-instructions*    
  '((("J" "NEAR") (:|imm|) (#xF #x80 :|+cc| :|rw/rd|)) 
    (("J") (:|imm|) (#x70 :|+cc| :|rb|)) 
    (("SET") (:|r/m8|) (#xF #x90 :|+cc| :/2)) 
    (("CMOV") (:|reg32| :|r/m32|) (:|o32| #xF #x40 :|+cc| :|/r|)) 
    (("CMOV") (:|reg16| :|r/m16|) (:|o16| #xF #x40 :|+cc| :|/r|))))

(defparameter *condition-codes* 
  '((:0 . 0) (:NO . 1) 
    (:B . 2) (:C . 2) (:NAE . 2)
    (:AE . 3) (:NB . 3) (:NC . 3)
    (:E . 4) (:Z . 4) (:NE . 5) (:NZ . 5)
    (:BE . 6) (:NA . 6) (:A . 7) (:NBE . 7)
    (:S . 8) (:NS . 9)
    (:P . 10) (:PE . 10) (:NP . 11) (:PO . 11)
    (:L . 12) (:NGE . 12) (:GE . 13) (:NL . 13)
    (:LE . 14) (:NG . 14) (:G . 15) (:NLE . 15)))

(defun process-cc-encoding (encoding conditional-value)
  (let ((cc-position 
	 (position :|+cc| encoding)))
    (loop
       for i from 0 below (length encoding)
       for code in encoding
       when (not (eq :|+cc| code))
       collect
	 (if (= i (1- cc-position))
	     (+ conditional-value code)
	     code))))

(defun process-conditional-instruction (instruction)
  (format t "Processing instruction ~A~&" instruction)
  (loop for conditional in *condition-codes*
     collect
       (list
	(list 
	 (if (= 1 (length (first instruction)))
	     (concatenate 'string 
			  (first (first instruction)) 
			  (symbol-name (car conditional)))
	     (concatenate 'string 
			  (first (first instruction)) 
			  (symbol-name (car conditional))
			 " "
			 (second (first instruction)))))
	(list 
	 (first (rest instruction))
	 (process-cc-encoding 
	  (second (rest instruction))
	  (cdr conditional))))))

(defun process-conditional-instructions ()
  (let ((result 
	 (loop
	    for instruction in *conditional-instructions*
	    append
	      (process-conditional-instruction instruction)))
	(*print-radix* t)
	(*print-base*  16))
    (loop 
       for instruction in result
       do
	 (format t  "~S~&" (list (first instruction) 
				 (first (second instruction)) 
				 (second (second instruction)))))))