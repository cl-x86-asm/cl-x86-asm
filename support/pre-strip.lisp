
(asdf:oos 'asdf:load-op 's-xml)
(asdf:oos 'asdf:load-op 'cl-ppcre)



(defun extract-pres (form)
  (loop
     for element in (cadr form)
     when (and (listp element) (equal (car element) :|pre|))
     collect (cdr element)))

(defun split-instructions (form)
  (loop
     for element in form
     collect (cl-ppcre::split (make-string 1 :initial-element #\Newline) (car element))))


(defun flatten-instructions (form)
  (let ((result nil))
    (loop
       for element in form
       do
	 (if (listp element)
	     (loop
		for sub-element in element
		do
		  (push sub-element result))
	     (push element result)))
    result))

(defun filter-comments (form)
  (loop
     for element in form
     unless (or (null (position #\; element))  
		 (char= (char element 0) #\Space))
     collect element))

(defun parse-instructions (form)
  (loop
     for element in form
     collect (cl-ppcre:split (make-string 1 :initial-element #\;) element)))


(defun parse-descriptions (form)
  (loop
     for (instruction description) in form
     collect (cons (string-trim " " instruction)
		   (cl-ppcre:split "\\[" description))))


(defun trim-descriptions (form)
  (loop
     for (instruction description processor) in form
     collect (list instruction (string-trim " " description) 
		   (concatenate 'string "["  processor))))

(defun reduce-to-string-list ()
  (trim-descriptions
   (parse-descriptions
    (parse-instructions 
     (filter-comments 
      (flatten-instructions 
       (split-instructions 
	(extract-pres 
	 (s-xml::parse-xml-file "/home/johnc/projects/msc/nasm-inst-list.xml")))))))))


(defun but-last-element (seq)
  (subseq seq 0 (1- (length seq))))

(defun last-element (seq)
  (subseq seq (1- (length seq))))


(defun process-opcodes (opcode-string)
  (flet ((hex-integer-or-string (str)
	   (let ((numeric-result
		  (parse-integer str :radix 16 :junk-allowed t)))
	     (or numeric-result str))))
    (let ((last-element nil))
      (loop
	 for element in
	   (loop
	      for element in (cl-ppcre::split " |(\\+)" opcode-string :with-registers-p t)
	      when element
	      collect
		(let ((result
		       (if (not (string= element "+"))
			   (if (string= last-element "+")
			       (concatenate 'string last-element element)
			       (hex-integer-or-string element))
			   nil)))		 
		  (setf last-element element)
		  result))
	 when element
	 collect element))))


     

;; given a string of the form "JMP FAR cc,nn,zz" split it into
;; instruction and opcode part
(defun destructure-instruction-string (instruction-string)
  (let* ((destructured-instruction 
	  (cl-ppcre::split " " instruction-string)))
    (if (= 1 (length destructured-instruction))
	(list destructured-instruction nil)
	(list (but-last-element destructured-instruction)
	      (last-element destructured-instruction)))))	      

(defun reduce-to-instruction-description (form)
  (destructuring-bind (instruction opcodes kind) form
    (destructuring-bind (instruction-name instruction-types)
	(destructure-instruction-string instruction)
      (destructuring-bind (&optional instruction-opcode-0-type instruction-opcode-1-type instruction-opcode-2-type)
	  (cl-ppcre::split ","  (first instruction-types))
	(list instruction-name 
	      (list instruction-opcode-0-type instruction-opcode-1-type instruction-opcode-2-type)
	      (process-opcodes opcodes)
	      (list kind))))))

(defparameter *instruction-descriptions*
   (loop 
      for element in (reduce-to-string-list)
      collect
	(reduce-to-instruction-description element)))

(defparameter *opcode-types* nil)
(defparameter *opcode-encodings* nil)

(defun intern-opcode-types (opcode-types)
  (loop
     for opcode-type in opcode-types
     when opcode-type
     collect
       (let
	   ((opcode-type-sym (intern opcode-type "KEYWORD")))
	 (unless
	     (member opcode-type-sym *opcode-types*) 
	   (push opcode-type-sym *opcode-types*))
	 opcode-type-sym)))

(defun intern-opcode-encodings (opcode-encodings)
  (loop
     for opcode-encoding in opcode-encodings
     when opcode-encoding
     collect
       (if (numberp opcode-encoding)
	 opcode-encoding
	 (let ((opcode-encoding-sym (intern opcode-encoding "KEYWORD")))
	       (unless 
		   (member opcode-encoding-sym *opcode-encodings*)
		 (push opcode-encoding-sym *opcode-encodings*))
	       opcode-encoding-sym))))

(defun print-instruction-descriptions ()
 (let ((*print-radix* t)
       (*print-base* 16))
   (format t "(defparameter  *instruction-table* ~% \'((")
   (loop
      for element in *instruction-descriptions*
      do (format t " ~%(~S ~S ~S)" 
		 (first element) 
		 (intern-opcode-types (second element)) 
		 (intern-opcode-encodings (third element)))))
 (format t "))~%")
 (format t "(defparameter *opcode-encodings* ~S)~%~%" *opcode-encodings*)
 (format t "(defparameter *opcode-types* ~S)~%~%" *opcode-types*)
 (format t "(defparameter *opcode-type-checkers*~%  (list")
 (loop
    for opcode-type in *opcode-types*
    do
      (format t "  (cons ~W ~%     (list (function (lambda (opcode) (equ ~W opcode)))))~%" opcode-type opcode-type))
 (format t "))")
 (finish-output))







  
