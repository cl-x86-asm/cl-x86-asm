
(asdf:oos 'asdf:load-op 'cl-ppcre)

(defparameter *opcode-modifiers* nil)

(defun collect-opcode-modifiers (keywords)
  (mapcar #'(lambda (opm) 
	      (unless (member opm *opcode-modifiers*) 
		(push opm *opcode-modifiers*))) keywords))

(defparameter *opcode-types* nil)
  
(defun collect-opcode-types (keywords)
  (mapcar #'(lambda (opt) 
	      (unless (member opt *opcode-types*) 
		(push opt *opcode-types*))) keywords))

(defparameter *string-scanner*
  (cl-ppcre:create-scanner
   '(:SEQUENCE "\"" 
     (:GREEDY-REPETITION 1 NIL 
      (:CHAR-CLASS (:RANGE #\A #\Z) (:RANGE #\0 #\9))) 
     "\"")))

(defparameter *hex-scanner*
  (cl-ppcre::create-scanner
   '(:SEQUENCE 
     (:CHAR-CLASS (:RANGE #\A #\F) (:RANGE #\0 #\9))
     (:CHAR-CLASS (:RANGE #\A #\F) (:RANGE #\0 #\9)))))

(defparameter *keyword-scanner*
  (cl-ppcre::create-scanner
   '(:SEQUENCE 
     (:GREEDY-REPETITION 1 NIL (:INVERTED-CHAR-CLASS #\Space #\Tab #\)))))) 

(defparameter *comment-scanner*
  (cl-ppcre::create-scanner
   '(:SEQUENCE
     :START-ANCHOR
     (:GREEDY-REPETITION 1 NIL :WHITESPACE-CHAR-CLASS)
     ";;"
     (:GREEDY-REPETITION 1 NIL :WHITESPACE-CHAR-CLASS)
     (:GREEDY-REPETITION 1 NIL (:CHAR-CLASS (:RANGE #\A #\Z) (:RANGE #\0 \#9)))
     (:GREEDY-REPETITION 1 NIL :WHITESPACE-CHAR-CLASS)
     (:REGISTER (:GREEDY-REPETITION 0 NIL 
		 (:INVERTED-CHAR-CLASS #\sPACE #\Tab #\=)))
     (:GREEDY-REPETITION 0 NIL :WHITESPACE-CHAR-CLASS)
     (:REGISTER (:GREEDY-REPETITION 0 NIL
		 (:INVERTED-CHAR-CLASS #\Space #\Tab #\=))))))


(defun extract-hex-digits (line start)
  (let ((last-match-end (1- (length line))))
    (values
     (loop
	:for scan-start = start then (cadr matches) 
	:for matches = (multiple-value-list 
			(cl-ppcre::scan *hex-scanner* line :start scan-start))
	:while (car matches)
	:collect (parse-integer 
		  (subseq line (car matches) (cadr matches)) 
		  :radix 16 :junk-allowed nil)
	:when (cadr matches)
	:do (setf last-match-end (cadr matches)))
     last-match-end)))

(defun collect-keywords (line hex-string-end)
  (loop
     :for scan-start = hex-string-end then (cadr matches)
     :for matches = (multiple-value-list (cl-ppcre::scan *keyword-scanner* line :start scan-start))
     :while (car matches)
     :collect (intern (subseq line (car matches) (cadr matches)) "KEYWORD")))

(defun process-operands (operand0 operand1)
    (if (and (zerop (length operand0))
	     (zerop (length operand1)))
	     nil
	     (if (zerop (length operand1))
		 (list (intern operand0 "KEYWORD"))
		 (list (intern operand0 "KEYWORD") 
		       (intern operand1 "KEYWORD")))))
	    
(defun process-instruction (line operands)
  (multiple-value-bind 	(string-start string-end)
      (cl-ppcre::scan *string-scanner* line)
    (let* ((instruction-name
	    (subseq line string-start string-end)))
      (multiple-value-bind (hex-digits hex-string-end)
	  (extract-hex-digits line string-end)
	(let ((keywords
	       (collect-keywords line hex-string-end)))
	  (let ((*print-base* 16)
		(*print-radix* t))
	    (if (null operands)
		(format t "(~A ~S ~S) ~%" 
			instruction-name hex-digits keywords)	
		(format t "(~A ~S ~S ~S) ~%" 
			instruction-name hex-digits keywords operands))
	    (if (not (null keywords))
		(collect-opcode-modifiers keywords))
	    (if (not (null operands))
		(collect-opcode-types operands))))))))

(defun collect-file (filename)
  (with-open-file (s filename)	   
    (loop 
       :for line = (list (read-line s) (read-line s)) then (list (read-line s nil nil) (read-line s nil nil))
       :while (car line)
       :collect line)))

(defun process-file (filename)
  (setf *opcode-types* nil)
  (setf *opcode-modifiers* nil)
  (format t "(defparameter  *instruction-table* ~% \'(")
  (let ((instructions-list        
	 (collect-file filename)))
    (mapcar #'(lambda (line)
		(let ((comment-line (car line))
		      (instruction-line (cadr line)))
		  (cl-ppcre::register-groups-bind 
		   (operand0 operand1) 
		   (*comment-scanner* comment-line) 
		   (process-instruction 
		    instruction-line 
		    (process-operands operand0 operand1))))) 
	    instructions-list)
    (finish-output))
  (format t "))~%")
  (format t "(defparameter *opcode-modifiers* ~S)~%" *opcode-modifiers*)
  (format t "(defparameter *opcode-types* ~S)~%" *opcode-types*)
  (format t "(defparameter *opcode-type-checkers*~%  (list")
  (loop
     for opcode-type in *opcode-types*
     do
     (format t "(cons ~S (list (function (lambda (opcode) t))))~%" opcode-type))
  (format t "))")
  (finish-output))
  
(process-file #P"x86-instruction-list.txt")