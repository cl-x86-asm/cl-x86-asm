

(in-package :cl-x86-asm)

;; x86 can assemble in one of two modes BITS32 or BITS16
(defparameter *current-bits-size* 32)

;; switch between modes
(defun switch-bits ()
  "(switch-bits) switch assembly mode between 32 bits and 16"
  (if (= *current-bits-size* 32)
      (setf *current-bits-size* 16)
    (setf *current-bits-size* 32)))

;; -- the actual assembler -----------------------------------------------------

;; to do -- lookup across different segments?
;; to do -- character operands?
(defun operand-value (operand)
  "(operand-value operand) - given operand which may be a number or a symbol
then reduce to it's numeric value"
  (cond
   ((numberp operand)
    operand)
   ((symbolp operand)
    (let ((result
           (find-symbol (symbol-name operand)
                        *current-segment-package*)))
      (if result
          (symbol-value result)
        (let ((result
               (find-symbol
                (symbol-name operand) 'keyword)))
          (if result
              result
            (error "Invalid operand ~A " operand))))))
   (t (error "Invalid operand ~A " operand))))

(defun operands-match-types-p (operands types)
  "(is-operand-type-p operand-type operand) Given an operand type (eg :reg32)
and an operand (eg :EAX), returns t if that operand is of the correct type"
  (labels
      ((operand-matches-type-p (op ty)
                               (funcall
                                (cadr (assoc ty *operand-type-checkers*))
                                (operand-value op))))
    (reduce
     #'(lambda (a b) (and a b))
     (mapcar
      #'operand-matches-type-p operands types))))

(defun encoding-length (encoding)
  "(encoding-length encoding) -- return the 'length' or weight of an encoding
  used when disambiguating potentially ambigous encodings"
  (+ (length encoding)
     (if (eq :ib (first (last encoding))) 1 0)
     (if (eq :iw (first (last encoding))) 2 0)
     (if (eq :id (first (last encoding))) 4 0)))

(defun one-description-p
  (descriptions)
  "(one-description-p descriptions-list) Quickie predicate to
check if we have one description left in our candidate list"
  (= (length descriptions) 1))

(defun filter-descriptions-by-number (operands descriptions)
  "(filter-descriptions-by-number operands descriptions) Given an
  operand list and a list of possible descriptions: pairs then
  return only those where the number of operands in the
  descriptions matches the number of operands given"
  (loop
   for (types encoding) in descriptions
   when (or (= (length types) (length operands))
            (and (zerop (length operands)) (eq (first types) :|none|)))
   collect (list types encoding)))

(defun filter-descriptions-by-type (operands descriptions)
  "(filter-descriptions-by-type operands descriptions) Return
only those descriptions that match the types of the operands"
  (loop
   for (types encoding) in descriptions
   when (operands-match-types-p operands types)
   collect (list types encoding)))

(defun filter-descriptions-by-simplicity (operands descriptions)
  "(pick-simplest-description descriptions) Look for a description with an operand
type that is a literal register keyword - an automatic win"
  (declare (ignore operands) (optimize (debug 3) (safety 1) (compilation-speed 0) (speed 0)))
  (if
      (loop
       for (types encoding) in descriptions
       for index = 0 then (1+ index)
       when (contains-keyword '(:STO :CX :ECX :DX :CL :AL :AX :EAX)  encoding)
       collect (list types encoding))
      descriptions)) ;; if we don't find anything, pass along to the next filter


(defun pick-smallest-description (descriptions)
  "(filter-descriptions-by-type operands descriptions)
Return the description that is likely to result in the
smallest possible encoding"
  (let
      ((candidate-index 0))
    (loop
     for (types encoding) in descriptions
     for index = 0 then (1+ index)
     when (< (encoding-length encoding)
             (encoding-length
              (second (nth index descriptions))))
     do
     (setf candidate-index index))
    (nth candidate-index descriptions)))

;; to do ? what if we *still* have alternate encodings
;; adfter going through *those* filters?

(defun find-correct-description (operands
                                 candidate-descriptions)
  "(find-correct-description operands candidate-descrptions)
Given a list of operands for an instruction and a list of possible
descriptions as a list of (types encoding) pair lists then find
the description which most likely satisfies the operands"
  (macrolet ((with-description-filter 
              (filterfn &rest body)
              `(let ((candidate-descriptions
                      (apply  ,filterfn (list operands candidate-descriptions))))
                 (format *debug-io* "Filtered Descriptions ~A~&"
                         candidate-descriptions)
                 (if (one-description-p candidate-descriptions)
                     (first candidate-descriptions)
                   ,@body))))
    (format *debug-io*
            "Operands ~A ~&Possible Descriptions ~A~&"
            operands candidate-descriptions)
    (with-description-filter
     #'filter-descriptions-by-number
     (with-description-filter
      #'filter-descriptions-by-type
      (with-description-filter
       #'filter-descriptions-by-simplicity
       (first candidate-descriptions))))))


(defun process-opcodes (insn-name operands types encoding instruction)
  (format *debug-io* "Processing instruction ~A~&" insn-name)
  (format *debug-io* "With operands ~A~&" operands)
  (format *debug-io* "Matching types ~A~&" types)
  (format *debug-io* "Using encoding ~A~&" encoding)
  (loop
   for code in encoding
   do
   (if (numberp code)
       (setf (get instruction :opcodes)
             (append (get instruction :opcodes) (list code)))
     (progn (format *debug-io* "~A~&" (gethash code *opcode-encoder-table*))
            (funcall (gethash code *opcode-encoder-table*)
                     insn-name operands types encoding instruction)))))
;;
;; turn an encoding into a sequcnce of bytes with no symbols
;;
(defun process-encoding (insn-name operands description)
  "(process-encoding instruction operand operand-description) Given an instruction,
 it's operands and it's description produce a stream of bytes for assembly, taking
it's encoding into account"
  (let ((result (make-instruction)))
    (format *debug-io*
            "Processing encoding of instruction ~A w operands ~A using description ~A~&"
            insn-name operands description)
    (process-opcodes insn-name operands
                     (first description) (second description)
                     result)
    (print-instruction "Resulting " result)
    (flatten-instruction result)))

;; to do -- need to put instructions in keyword package,
;; hash-table to lookup
(defun assemble-instruction-or-directive (instruction operands)
  "(assemble-instruction-or-directive instruction operand) Given the itstruction
and operand portion of an assembler form, assemble it in the current segment"
  ;; deal with instruction
  (format *debug-io* "Looking up instruction ~A~&" instruction)
  (multiple-value-bind (instruction operand-descriptions)
      (lookup-instruction instruction)
    (when operand-descriptions
      (format *debug-io* "Found instruction ~A~&" instruction)
      (let ((candidate-description
             (find-correct-description operands operand-descriptions)))
        (emit-bytes-to-segment
         (process-encoding
          instruction
          operands
          candidate-description))))))



(defun assemble-form (form)
  "(assemble-form form) -- assemble a single line of assembler code
The form may be one of the following:
a number - a literal to be emitted
a symbol - if it is in the keyword packaage it is an instruction with no operands
         - if it is begins with a dot it is a symbol definition
         - if does not begin with a dot it is a symbol reference
a list   - it is a list of the above, plus possibly operands"
  (labels
      ;; remove dot from the front of a symbol
      ((remove-dot (x)
                   (subseq x 1 (length x)))
       ;; test to see if a symbol begins with a dot
       (begins-with-dot (x)
                        (char= (char (symbol-name x) 0) #\.)))
    (format *debug-io* "--~&Assembling form ~A~%" form)
    (ctypecase form
               ;; value is supposed to be a literal to be poked in
               (number (emit-data-to-segment form))
               ;; symbol is supposed to be a reference to a symbol,
               ;; or a definition of a symbol unless its a keyword
               ;; in which case it's an opcode-less directive
               (symbol
                (if (symbol-is-in-package form "KEYWORD")
                    (assemble-instruction-or-directive form nil)
                  (if (begins-with-dot form)
                      (add-symbol-definition (remove-dot (symbol-name form)))
                    (add-symbol-reference (symbol-name form)))))
               (list
                ;; if the list begins with a keyword, the rest of it
                ;; is an instruction
                (if (and (symbolp (first form))
                         (symbol-is-in-package (first form) "KEYWORD"))
                    (assemble-instruction-or-directive (first form) (rest form))
                  (progn
                    (assemble-form (first form))
                    (when (rest form)
                      (assemble-form (rest form)))))))))


(defun assemble-forms (&rest forms)
  "(assemble-forms forms)
Assemble a list of forms in the current segment"
  (mapcar #'assemble-form forms))



