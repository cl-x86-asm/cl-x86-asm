;;;; Silly emacs, this is -*- Lisp -*-

(in-package :asdf)

(defsystem :cl-x86-asm
    :name "cl-x86-asm"
    :author "John Connors"
    :version "1.0"
    :licence "MIT"
    :description "Portable X86 assembler in Common Lisp"
    :components ((:file "package")		 
		 (:file "x86instructions" 
			:depends-on ("package"))
		 (:file "utils" 
			:depends-on ("package"))
		 (:file "symbols" 
			:depends-on ("package" 
				     "utils"))
		 (:file "syntax"
			:depends-on ("package"
				     "utils"
				     "symbols"))
		 (:file "x86opcodes" 
			:depends-on ("package" 
				     "utils" 
				     "symbols"
				     "syntax"))
 		 (:file "x86assembler" 
			:depends-on ("package" 
				     "x86instructions" 
				     "utils" 
				     "symbols"
				     "x86opcodes" 
				     "syntax"))))
