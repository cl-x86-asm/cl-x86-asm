
(in-package :cl-user)

(defpackage :cl-x86-asm 
  (:use :cl)
  (:nicknames :x86-asm)
  (:export
   :make-instruction-hashtable
   :assemble-form
   :assemble-forms
   :make-segment
   :assemble-forms
   :print-segment))

(in-package :cl-x86-asm)
